/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teerarak.oxfuntion;

import java.util.Scanner;

/**
 *
 * @author AuyouknoW
 */
public class OXFuntion {
    static int round = 0;
    static char win = '-';
    static boolean isFinish = false;
    static int row, col;
    static boolean winner = false;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void Input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
            isFinish = true;
            break;
        }
    }

    static void checkCol() {
        for (row = 0; row < 3; row++) {
            if (table[0][row] == player && table[1][row] == player
                    && table[2][row] == player) {
                win = player;
                winner = true;
                isFinish = true;
                break;
            }
        }
    }
     static void checkscalene() {
       if (((table[0][0] == player && table[1][1] == player && table[2][2] == player)
                || (table[0][2] == player && table[1][1] == player && table[2][0] == player))) {
            win = player;
            winner = true;
            isFinish = true;
        }
    }
    static boolean checkDraw(){
         if (round == 9) {
            return true;
        }else{
        return false;
         }
    }
    static void checkWin() {
        checkCol();
        checkscalene();
        checkDraw();
        round++;
    }

    static void SwitchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
          System.out.println("Player " + player + " Win....");
    }

    static void showBye() {
        System.out.println("Bye Bye...");
    }

    public static void main(String[] args) {
        showWelcome();
            do {
                showTable();
                showTurn();
                Input();
                checkWin();
                if (checkDraw()) {
                    showTable();
                    System.out.println("Draw !!!");
                    break;
                }
                SwitchPlayer();

            } while (!isFinish);

            if (!checkDraw()) {
                showTable();
                showResult();
            }
            showBye();


}
